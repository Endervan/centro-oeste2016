<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 9) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 143px center no-repeat;
  }
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row top85">

      <div class="col-xs-12 fale_conosco">
        <h1>ENTRE EM CONTATO SEMPRE QUE PRECISAR
          <span class="clearfix">TRABALHE CONOSCO</span>
        </h1>
      </div>

  </div>
 </div>




 <div class="container">
   <div class="row top70 pb80 bg_branco_dicas">


     <!-- ======================================================================= -->
     <!--CONTATOS   -->
     <!-- ======================================================================= -->
     <div class="col-xs-6 top40 padding0">
         <div class="media produtos_dentro_contatos">
           <div class="media-right pr15">
             <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone_produtos.png" alt=""/>
           </div>
           <div class="media-body">
             <h2>TELEFONE</h2>
             <h4 class="media-heading"><span><?php Util::imprime($config[ddd1]) ?></span>
               <?php Util::imprime($config[telefone1]) ?>
             </h4>
           </div>

         </div>
     </div>
     <!-- ======================================================================= -->
     <!--CONTATOS   -->
     <!-- ======================================================================= -->






     <!-- ======================================================================= -->
     <!--ENDERENCO   -->
     <!-- ======================================================================= -->
     <div class="col-xs-6 top40">
         <div class="media fale_endereco">
           <div class="media-right pr15">
             <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_mapa.png" alt=""/>
           </div>
           <div class="media-body">
             <h2>ENDEREÇO</h2>
             <h4 class="media-heading">
               <?php Util::imprime($config[endereco]) ?>
             </h4>
           </div>

         </div>
     </div>
     <!-- ======================================================================= -->
     <!--ENDERENCO   -->
     <!-- ======================================================================= -->



     <div class="col-xs-6 top90 dicas_geral">
       <h1>ENVIE UM E-MAIL</h1>




             <div class="top10">

               <?php
             //  VERIFICO SE E PARA ENVIAR O EMAIL
             if(isset($_POST[nome]))
             {

               if(!empty($_FILES[curriculo][name])):
                 $nome_arquivo = Util::upload_arquivo("./uploads", $_FILES[curriculo]);
                 $texto = "Anexo: ";
                 $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
                 $texto .= "<a href='".Util::caminho_projeto()."/uploads/$nome_arquivo' target='_blank'>".Util::caminho_projeto()."/uploads/$nome_arquivo</a>";
               endif;

                     $texto_mensagem = "
                                       Nome: ".$_POST[nome]." <br />
                                       Telefone: ".$_POST[telefone]." <br />
                                       Email: ".$_POST[email]." <br />
                                       Escolaridade: ".$_POST[escolaridade]." <br />
                                       Cargo: ".$_POST[cargo]." <br />
                                       Área: ".$_POST[area]." <br />
                                       Cidade: ".$_POST[cidade]." <br />
                                       Estado: ".$_POST[estado]." <br />
                                       Mensagem: <br />
                                       ".nl2br($_POST[mensagem])."

                                       <br><br>
                                       $texto
                                       ";


                     Util::envia_email($config[email], utf8_decode("$_POST[nome] enviou um currículo"), $texto_mensagem, utf8_decode($_POST[nome]), ($_POST[email]));
                     Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] enviou um currículo"), $texto_mensagem, utf8_decode($_POST[nome]), ($_POST[email]));
                     Util::alert_bootstrap("Obrigado por entrar em contato.");
                     unset($_POST);
             }

             ?>


             <form class="form-inline FormCurriculo" role="form" method="post" enctype="multipart/form-data">
               <div class="fundo_formulario">
                 <!-- formulario orcamento -->
                 <div class="top20">
                   <div class="col-xs-6">
                     <div class="form-group  input100 has-feedback">
                       <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">

                     </div>
                   </div>

                   <div class="col-xs-6">
                     <div class="form-group  input100 has-feedback">
                       <input type="text" name="email" class="form-control fundo-form1 input100 input-lg" placeholder="E-MAIL">

                     </div>
                   </div>
                 </div>

                 <div class="clearfix"></div>


                <div class="top20">
                   <div class="col-xs-6">
                     <div class="form-group input100 has-feedback ">
                       <input type="text" name="telefone" class="form-control fundo-form1 input100  input-lg" placeholder="TELEFONE">

                     </div>
                   </div>

                   <div class="col-xs-6">
                     <div class="form-group input100 has-feedback ">
                       <input type="text" name="escolaridade" class="form-control fundo-form1 input100  input-lg" placeholder="ESCOLARIDADE">

                     </div>
                   </div>
                 </div>

                 <div class="clearfix"></div>

                 <div class="top20">
                   <div class="col-xs-6">
                     <div class="form-group input100 has-feedback">
                       <input type="text" name="cargo" class="form-control fundo-form1 input100  input-lg" placeholder="CARGO" >

                     </div>
                   </div>

                   <div class="col-xs-6">
                     <div class="form-group input100 has-feedback ">
                       <input type="text" name="area" class="form-control fundo-form1 input100  input-lg" placeholder="AREA">

                     </div>
                   </div>
                 </div>

                  <div class="clearfix"></div>

                 <div class="top20">

                   <div class="col-xs-6">
                     <div class="form-group input100 has-feedback ">
                       <input type="text" name="cidade" class="form-control fundo-form1 input100  input-lg" placeholder="CIDADE">

                     </div>
                   </div>
                   <div class="col-xs-6">
                     <div class="form-group input100 has-feedback ">
                       <input type="text" name="estado" class="form-control fundo-form1 input100  input-lg" placeholder="ESTADO">

                     </div>
                   </div>

                 </div>


                 <div class="clearfix"></div>

                  <div class="top20">
                   <div class="col-xs-12">
                     <div class="form-group input100 has-feedback ">
                       <input type="file" name="curriculo" class="form-control fundo-form1 input100  input-lg" placeholder="CURRÍCULO">

                     </div>
                   </div>
                   </div>

                 <div class="clearfix"></div>


                 <div class="top20">
                   <div class="col-xs-12">
                    <div class="form-group input100 has-feedback">
                     <textarea name="mensagem" cols="30" rows="9" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
                     
                   </div>
                 </div>
               </div>


               <!-- formulario orcamento -->
               <div class="col-xs-12 text-right">
                 <div class="top15">
                   <button type="submit" class="btn btn_formulario" name="btn_contato">
                     ENVIAR
                   </button>
                 </div>
               </div>

             </div>


             <!--  ==============================================================  -->
             <!-- FORMULARIO-->
             <!--  ==============================================================  -->
           </form>
           </div>
           </div>

           <!--  ==============================================================  -->
           <!--MAPA-->
           <!--  ==============================================================  -->
           <div class="col-xs-6">
             <div class="top90 bottom20 dicas_geral">
               <h1>COMO CHEGAR</h1>
             </div>
             <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="405" frameborder="0" style="border:0" allowfullscreen></iframe>
           </div>
           <!--  ==============================================================  -->
           <!--MAPA-->
           <!--  ==============================================================  -->
   </div>
 </div>



<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>

<script>
  $(document).ready(function() {
    $('.FormCurriculo').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
         validators: {
           notEmpty: {

           },
           phone: {
               country: 'BR',
               message: 'Informe um telefone válido.'
           }
         },
       },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      escolaridade: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5*1024*1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                          }
                        }
                      },
                      mensagem: {
                        validators: {
                          notEmpty: {

                          }
                        }
                      }
                    }
                  });
  });
</script>
