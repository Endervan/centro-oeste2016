<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

  <!-- slider JS files -->
  <script  src="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/jquery-1.8.0.min.js"></script>
  <link href="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/royalslider.css" rel="stylesheet">
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/jquery.royalslider.min.js"></script>

  <link href="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/minimal-white/rs-minimal-white.css" rel="stylesheet">

  <script>
    jQuery(document).ready(function($) {
      // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
      // it's recommended to disable them when using autoHeight module
      $('#content-slider-1').royalSlider({
        autoHeight: true,
        arrowsNav: true,
        arrowsNavAutoHide: false,
        keyboardNavEnabled: true,
        controlNavigationSpacing: 0,
        controlNavigation: 'tabs',
        autoScaleSlider: false,
        arrowsNavAutohide: true,
        arrowsNavHideOnTouch: true,
        imageScaleMode: 'none',
        imageAlignCenter: false,
        fadeinLoadedSlide: true,
        loop: false,
        loopRewind: true,
        numImagesToPreload: 6,
        keyboardNavEnabled: true,
        usePreloader: false,
        autoPlay: {
              // autoplay options go gere
              enabled: true,
              pauseOnHover: true
            }

          });
    });
  </script>


<script>
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: true,
    itemWidth: 386,
    itemMargin: 0,
     minItems: 1,
    maxItems: 5
  });
});
</script>


</head>
<body>


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!-- BG HOME    -->
  <!-- ======================================================================= -->
  <div class="container-fluid">
    <div class="row">

      <div id="container_banner">

        <div id="content_slider">
          <div id="content-slider-1" class="contentSlider rsDefault">

            <?php
            $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 ORDER BY rand() LIMIT 4");

            if (mysql_num_rows($result) > 0) {
              while ($row = mysql_fetch_array($result)) {
                ?>
                <!-- ITEM -->
                <div>
                  <?php
                  if ($row[url] == '') {
                    ?>
                    <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="1000" alt=""/>
                    <?php
                  }else{
                    ?>
                    <a href="<?php Util::imprime($row[url]) ?>">
                      <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="1000" alt=""/>
                    </a>
                    <?php
                  }
                  ?>

                </div>
                <!-- FIM DO ITEM -->
                <?php
              }
            }
            ?>

          </div>
        </div>
        <div class="bg_branco"></div>
      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- BG HOME    -->
  <!-- ======================================================================= -->



  <!-- categorias e busca de produtos -->
  <div class="container-fluid relativo">
    <div class="row">
      <div class="bg_menu_produtos"></div>

      <div class="container top50">
        <div class="row">

          <div class="col-xs-5">
            <div class="media">
              <div class="media-body text-right">
                <h4 class="media-heading">CONHEÇA NOSSA
                  <span class="clearfix">LINHA DE PRODUTOS</span>
                </h4>
              </div>

              <div class="media-right">
                <i class="fa fa-angle-double-down fa-5x media-object"></i>
              </div>

            </div>
          </div>


            <div class="col-xs-7 padding0">

              <ul class="sub-menu-produtos left9 sub-menu-produtos-index">

                <?php
                $result = $obj_site->select("tb_categorias_produtos", "order by ordem limit 0, 4");
                if (mysql_num_rows($result) > 0) {
                  while($row = mysql_fetch_array($result)){
                    ?>
                    <li>
                      <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                        <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>">
                        <span class="clearfix"><?php Util::imprime($row[titulo]); ?></span>
                      </a>
                    </li>
                    <?php
                  }
                }
                ?>



              </ul>

            </div>



          </div>
        </div>
        <!-- categorias e busca de produtos -->

      </div>
    </div>








 <!-- ======================================================================= -->
 <!-- PRODUTOS HOME    -->
 <!-- ======================================================================= -->
 <div class='container '>
   <div class="row">

    <?php
	$result = $obj_site->select("tb_produtos", "order by rand() limit 8");
	if(mysql_num_rows($result) > 0){
    	while ($row = mysql_fetch_array($result)) {
    	?>
    		<div class="col-xs-3 produtos_home top35">
			  <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]) ?>" title="">
				  <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 270, 270, array("class"=>"input100", "alt"=>"$row[titulo]") ) ?>
			  </a>
			  <h1 class="top10"><?php Util::imprime($row[titulo]) ?></h1>
			  <h2><?php Util::imprime($row[modelo]) ?></h2>

			  <a class="btn btn_saiba_mais top15" href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]) ?>" data-toggle="tooltip" data-placement="top" title="Saiba Mais">
				SAIBA MAIS
			  </a>
			  <a class="btn btn_orcamentos_home top15 left5" href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'" >
				<i class="fa fa-cart-arrow-down fa-2x " aria-hidden="true"></i>
			  </a>
			</div>
    	<?php
    	}
    }
    ?>



     </div>
   </div>
 <!-- ======================================================================= -->
 <!-- PRODUTOS HOME    -->
 <!-- ======================================================================= -->



 <!-- ======================================================================= -->
 <!--SERVICOS HOME    -->
 <!-- ======================================================================= -->
<div class="container-fluid bg_servicos_home">
    <div class="row">

      <div class="container top150">
        <div class="row">

          <div class="col-xs-12">
            <div class="media pull-right">
              <div class="media-body text-right">
                <h4 class="media-heading">CONFIRA
                  <span class="clearfix">NOSSOS SERVIÇOS</span>
                </h4>
              </div>

              <div class="media-right">
                <i class="fa fa-angle-double-down fa-5x media-object"></i>
              </div>

            </div>
          </div>


          <!-- ======================================================================= -->
          <!--TIPOS SERVICOS HOME  ate 4 itens home  -->
          <!-- ======================================================================= -->
          <div class="col-xs-7 col-xs-offset-5 servicos_home top10">


            <?php
            $result = $obj_site->select("tb_servicos", "order by rand() limit 4");
            if (mysql_num_rows($result) > 0) {
            	while ($row = mysql_fetch_array($result)) {
            	?>
            		<a href="<?php echo Util::caminho_projeto(); ?>/servico/<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]) ?>">
						<div class="media top15">
						  <div class="media-left media-middle">
							  <i class="fa fa-check-square-o media-object"></i>
						  </div>
						  <div class="media-body">
							<p class="media-heading"><?php Util::imprime($row[titulo]) ?></p>
						  </div>
						</div>
           			</a>
            	<?php
            	}
            }
            ?>



          </div>

          <div class="col-xs-7 col-xs-offset-5 servicos_home01 top40">
            <div class="media">
              <div class="media-body text-right">
                <h4 class="media-heading">CONHEÇA
                  <span class="clearfix">NOSSA EMPRESA</span>
                </h4>
              </div>

              <div class="media-right">
                <i class="fa fa-angle-double-down fa-5x media-object"></i>
              </div>
            </div>

            <div class="top25">
                <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3) ?>
                <p><?php Util::imprime($row[descricao]) ?></p>
            </div>

            <div class="top40 text-right">
                <h1>NOSSOS CLIENTES<i class="fa fa-angle-double-down"></i></h1>
            </div>

        </div>

        </div>
      </div>
    </div>
</div>
 <!-- ======================================================================= -->
 <!-- SERVICOS HOME    -->
 <!-- ======================================================================= -->


 <!-- ======================================================================= -->
 <!--SERVICOS HOME REALIZADOS    -->
 <!-- ======================================================================= -->
 <div class="container-fluid servicos_realizados">
    <div class="row">

      <!-- Place somewhere in the <body> of your page -->
        <div class="flexslider">
          <ul class="slides">
            <?php
            $result = $obj_site->select("tb_clientes", "order by rand() limit 10");
            if (mysql_num_rows($result) > 0) {
            	while ($row = mysql_fetch_array($result)) {
            	?>
            		 <li>
					  	<a href="<?php echo Util::caminho_projeto(); ?>/empresa" title="<?php Util::imprime($row[titulo]) ?>">
					  		<?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 386, 200, array("class"=>"", "alt"=>"$row[titulo]") ) ?>
					  	</a>
					 </li>
            	<?php
            	}
            }
            ?>
          </ul>
        </div>

    </div>
 </div>
 <!-- ======================================================================= -->
 <!-- SERVICOS HOME REALIZADOS    -->
 <!-- ======================================================================= -->


 <!-- ======================================================================= -->
 <!--DICAS HOME   -->
 <!-- ======================================================================= -->
 <div class="container top50">
    <div class="row">

      <div class="col-xs-6">
        <div class="media">
          <div class="media-body text-right">
            <h4 class="media-heading">CONFIRA AS
              <span class="clearfix">NOSSAS DICAS</span>
            </h4>
          </div>

          <div class="media-right">
            <i class="fa fa-angle-double-down fa-5x media-object"></i>
          </div>

        </div>
      </div>

      <div class="col-xs-6 top15 text-right">
        <a class="btn btn_saiba_mais top15" href="<?php echo Util::caminho_projeto() ?>/dicas">
          VER TODAS
        </a>
      </div>
      <div class="clearfix">  </div>





      <!-- ======================================================================= -->
      <!-- item 01   max 03 itens  -->
      <!-- ======================================================================= -->
      <?php
      $result = $obj_site->select("tb_dicas", "order by rand() limit 3");
      if (mysql_num_rows($result) > 0) {
      	while ($row = mysql_fetch_array($result)) {
      	?>
      		<div class="col-xs-4 dicas_home top50">
				 <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]) ?>">
					 <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 360, 237, array("class"=>"input100", "alt"=>"$row[titulo]") ) ?>
				 </a>
				 <h1 class="top10"><?php Util::imprime($row[titulo]) ?></h1>

				 <a class="btn btn_saiba_mais top15" href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]) ?>">
				   SAIBA MAIS
				 </a>

			 </div>
      	<?php
      	}
      }
      ?>

       <!-- ======================================================================= -->
       <!-- item 01    -->
       <!-- ======================================================================= -->


    </div>
 </div>
 <!-- ======================================================================= -->
 <!-- DICAS HOME   -->
 <!-- ======================================================================= -->





  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



    </body>

    </html>
