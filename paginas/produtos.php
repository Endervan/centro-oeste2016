<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 4) ?>
<style>
    .bg-interna{
      background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 143px center no-repeat;
    }
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->

  <!--  ==============================================================  -->
  <!--  TITULO PAGINA -->
  <!--  ==============================================================  -->
  <div class="container">
     <div class="row top140">

       <div class="col-xs-8">
         <h3>NOSSOS PRODUTOS</h3>
       </div>

   </div>
  </div>
  <!--  ==============================================================  -->
  <!--  TITULO PAGINA -->
  <!--  ==============================================================  -->


  <!--  ==============================================================  -->
  <!--  MENU EMPRESA -->
  <!--  ==============================================================  -->
  <div class="container">
      <div class="row">
          <div class="col-xs-8">
              <ul class="sub-menu-produtos left45">



					<?php
					$result = $obj_site->select("tb_categorias_produtos", "order by ordem");
					if (mysql_num_rows($result) > 0) {
						while($row = mysql_fetch_array($result)){
							?>
							<li>
								<a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
									<img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>">
									<span class="clearfix"><?php Util::imprime($row[titulo]); ?></span>
								</a>
							</li>
							<?php
						}
					}
					?>






              </ul>
          </div>
      </div>
  </div>
  <!--  ==============================================================  -->
  <!--  MENU EMPRESA -->
  <!--  ==============================================================  -->




 <!-- ======================================================================= -->
 <!-- PRODUTOS GERAL    -->
 <!-- ======================================================================= -->
 <div class="container">
   <div class="row top90 pb50 bg_branco_dicas">

     <div class="col-xs-12 top40 produtos_geral">
       <h1>PRODUTOS EM DESTAQUE</h1>
     </div>

    <!-- ======================================================================= -->
    <!-- item 01   -->
    <!-- ======================================================================= -->
    <?php
	$url1 = Url::getURL(1);

	//  FILTRA AS CATEGORIAS
	if (isset( $url1 )) {
	  $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $url1);
	  $complemento .= "AND id_categoriaproduto = '$id_categoria' ";
	}



    $result = $obj_site->select("tb_produtos", $complemento);
	if(mysql_num_rows($result) == 0){
		echo "<div class='col-xs-12 top70 bottom70'> <p class='bg-danger' style='padding: 20px;'>Nenhum registro encontrado.</p> </div>";
	}else{
    	while ($row = mysql_fetch_array($result)) {
    	?>
    		<div class="col-xs-3 produtos_home top35">
			  <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]) ?>" title="">
				  <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 270, 270, array("class"=>"input100", "alt"=>"$row[titulo]") ) ?>
			  </a>
			  <h1 class="top10"><?php Util::imprime($row[titulo]) ?></h1>
			  <h2><?php Util::imprime($row[modelo]) ?></h2>

			  <a class="btn btn_saiba_mais top15" href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]) ?>" data-toggle="tooltip" data-placement="top" title="Adicionar ao orçamento">
				SAIBA MAIS
			  </a>
			  <a class="btn btn_orcamentos_home top15 left5" href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'" >
				<i class="fa fa-cart-arrow-down fa-2x " aria-hidden="true"></i>
			  </a>
			</div>
    	<?php
    	}
    }
    ?>
	<!-- ======================================================================= -->
	<!-- item 01    -->
	<!-- ======================================================================= -->


   </div>
 </div>
<!-- ======================================================================= -->
<!-- PRODUTOS GERAL    -->
<!-- ======================================================================= -->



<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>
