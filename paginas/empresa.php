<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 1) ?>
<style>
    .bg-interna{
      background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 143px center no-repeat;
    }
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->






 <!--  ==============================================================  -->
 <!--  TITULO PAGINA -->
 <!--  ==============================================================  -->
 <div class="container">
    <div class="row top140">

      <div class="col-xs-8 text-right">
        <h3 class="right40">NOSSA EMPRESA</h3>
      </div>

  </div>
 </div>
 <!--  ==============================================================  -->
 <!--  TITULO PAGINA -->
 <!--  ==============================================================  -->



 <!--  ==============================================================  -->
 <!--  MENU EMPRESA -->
 <!--  ==============================================================  -->
 <div class="container">
     <div class="row">
         <div class="col-xs-8">
             <ul class="sub-menu-dentro">
                 <li>
                     <a href="javascript:void(0);" onclick="$('html,body').animate({scrollTop: $('.loc-quem-somos').offset().top}, 2000);" title="QUEM SOMOS">
                         <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-quem-somos.jpg" alt="QUEM SOMOS">
                         <br>
                         QUEM SOMOS
                     </a>
                 </li>
                 <li>
                     <a href="javascript:void(0);" onclick="$('html,body').animate({scrollTop: $('.loc-servicos').offset().top}, 2000);" title="SERVIÇO DE ATENDIMENTO">
                         <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-servicos-atendimento.jpg" alt="SERVIÇO DE ATENDIMENTO">
                         <br>
                         SERVIÇO DE ATENDIMENTO
                     </a>
                 </li>

                 <li>
                     <a href="javascript:void(0);" onclick="$('html,body').animate({scrollTop: $('.loc-clientes').offset().top}, 2000);" title="CLIENTES">
                         <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-clientes.jpg" alt="CLIENTES">
                         <br>
                         CLIENTES
                     </a>
                 </li>
             </ul>
         </div>
     </div>
 </div>
 <!--  ==============================================================  -->
 <!--  MENU EMPRESA -->
 <!--  ==============================================================  -->






 <!-- ======================================================================= -->
 <!-- DICAS GERAL    -->
 <!-- ======================================================================= -->
 <div class="container">
     <div class="row top80 bg_branco_dicas dados-empresa ">

           <div class="col-xs-12 top35 loc-quem-somos">
               <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-quem-somos-2.jpg" alt="QUEM SOMOS" class="pull-left right10">
               <h5>QUEM SOMOS</h5>

               <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
               <p class="top40"><?php Util::imprime($row[descricao]) ?></p>

           </div>


           <div class="col-xs-12 top35 loc-servicos">
               <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-servicos-atendimento-2.jpg" alt="SERVIÇO DE ATENDIMENTO" class="top5 pull-left right10">
               <h5>SERVIÇO DE ATENDIMENTO</h5>

               <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2) ?>
               <p class="top40"><?php Util::imprime($row[descricao]) ?></p>

           </div>





           <div class="col-xs-12 top35 loc-clientes">
               <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-cliente-2.jpg" alt="CLIENTES" class="pull-left right10">
               <h5>CLIENTES</h5>
               <?php require_once("./includes/slider_clientes.php"); ?>
           </div>



     </div>
 </div>





















<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>
