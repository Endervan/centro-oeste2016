<?php  


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
   $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_produtos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/produtos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>


<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


  <!-- slider JS files -->
  <script  src="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/jquery-1.8.0.min.js"></script>
  <link href="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/royalslider.css" rel="stylesheet">
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/jquery.royalslider.min.js"></script>

  <link href="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/minimal-white/rs-minimal-white.css" rel="stylesheet">

  <script>
    jQuery(document).ready(function($) {
      // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
      // it's recommended to disable them when using autoHeight module
      $('#content-slider-11').royalSlider({
        autoHeight: true,
        arrowsNav: true,
        // transitionType: fade,
        arrowsNavAutoHide: false,
        keyboardNavEnabled: true,
        controlNavigationSpacing: 0,
        controlNavigation: 'tabs',
        autoScaleSlider: false,
        arrowsNavAutohide: true,
        arrowsNavHideOnTouch: true,
        imageScaleMode: 'none',
        imageAlignCenter: false,
        // fadeinLoadedSlide: true,
        loop: false,
        loopRewind: true,
        numImagesToPreload: 6,
        keyboardNavEnabled: true,
        usePreloader: false,
        autoPlay: {
              // autoplay options go gere
              enabled: true,
              pauseOnHover: true
            }

          });
    });
  </script>




  <script>
	  $(window).load(function() {
		$('.flexslider').flexslider({
		  animation: "slide",
		  animationLoop: true,
		  itemWidth: 540,
		  itemMargin: 40,
		  controlsContainer: $(".custom-controls-container"),
    	  customDirectionNav: $(".custom-navigation a")
		});
	  });
	  </script>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 5) ?>
<style>
    .bg-interna{
      background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 143px center no-repeat;
    }
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <div class="container">
    <div class="row top120">

      <div class="col-xs-12 produto_titulo">
        <h1><?php Util::imprime($dados_dentro[titulo]) ?></h1>
        <h2><?php Util::imprime($dados_dentro[modelo]) ?></h2>
      </div>

  </div>
 </div>




 <!-- ======================================================================= -->
 <!-- PRODUTOS    -->
 <!-- ======================================================================= -->
 <div class="container">
   <div class="row top80 pb50 bg_branco_dicas">

     <div class="col-xs-5 top40">
         <div class="media produtos_dentro">
           <div class="media-body text-right">
             <h4 class="media-heading">CONFIRA AS
               <span class="clearfix">INFORMAÇÕES DO PRODUTO</span>
             </h4>
           </div>
           <div class="media-right">
             <i class="fa fa-angle-double-down fa-4x media-object"></i>
           </div>
         </div>
     </div>

     <!-- ======================================================================= -->
     <!--CONTATOS   -->
     <!-- ======================================================================= -->
     <div class="col-xs-3 top40 padding0">
         <div class="media produtos_dentro_contatos">
           <div class="media-right pr15">
             <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone_produtos.png" alt=""/>
           </div>
           <div class="media-body">
             <h2>TELEFONE</h2>
             <h4 class="media-heading"><span><?php Util::imprime($config[ddd1]) ?></span>
               <?php Util::imprime($config[telefone1]) ?>
             </h4>
           </div>

         </div>
     </div>
     <!-- ======================================================================= -->
     <!--CONTATOS   -->
     <!-- ======================================================================= -->

     <!-- ======================================================================= -->
     <!--BTN ORCAMENTO   -->
     <!-- ======================================================================= -->
     <div class="col-xs-4 top20 ">
        <a title="SOLICITAR UM ORÇAMENTO" class="btn btn_orcamentos col-xs-12" href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'" >
          <i class="fa fa-shopping-cart pull-left"></i>
          <span class="pt15">SOLICITAR UM ORÇAMENTO</span>
        </a>
     </div>
     <!-- ======================================================================= -->
     <!--BTN ORCAMENTO   -->
     <!-- ======================================================================= -->

     <!-- ======================================================================= -->
     <!--CARROUCEL   -->
     <!-- ======================================================================= -->
     <div class="col-xs-6 top50 ">
       <div id="container_banner1" class="pull-left">

         <div id="content_slider1">
           <div id="content-slider-11" class="contentSlider rsDefault">

             <?php
             $result = $obj_site->select("tb_galerias_produtos", "AND id_produto = $dados_dentro[0]");

             if (mysql_num_rows($result) > 0) {
               while ($row = mysql_fetch_array($result)) {
                 ?>
                 <!-- ITEM -->
                 <div>
                   	 <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 550, 390, array("class"=>"input100 rsImg", "alt"=>"") ) ?>
                 </div>
                 <!-- FIM DO ITEM -->
                 <?php
               }
             }
             ?>

           </div>
         </div>
       </div>
     </div>
     <!-- ======================================================================= -->
     <!--CARROUCEL   -->
     <!-- ======================================================================= -->


     <div class="col-xs-6">
        <div class="top40">
          <p><?php Util::imprime($dados_dentro[descricao]) ?></p>
        </div>
     </div>

     

     <!-- ======================================================================= -->
     <!--BTN ORCAMENTO   -->
     <!-- ======================================================================= -->
     <div class="col-xs-4 top20 ">
        <a title="SOLICITAR UM ORÇAMENTO" class="btn btn_orcamentos col-xs-12" href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'" >
          <i class="fa fa-shopping-cart pull-left"></i>
          <span class="pt15">SOLICITAR UM ORÇAMENTO</span>
        </a>
     </div>
     <!-- ======================================================================= -->
     <!--BTN ORCAMENTO   -->
     <!-- ======================================================================= -->
     <div class="clearfix">  </div>


     <!-- ======================================================================= -->
     <!--TITULO PERSONALIZADOS  medio-->
     <!-- ======================================================================= -->
     <div class="col-xs-offset-7 col-xs-5">
       <div class="media titulo_medio pull-right">
         <div class="media-right">
           <i class="fa fa-angle-double-down fa-5x media-object"></i>
         </div>
         <div class="media-body">
           <h4 class="media-heading">CONFIRA OUTROS
             <span class="clearfix">PRODUTOS</span>
           </h4>
         </div>
       </div>
     </div>
     <!-- ======================================================================= -->
     <!--TITULO PERSONALIZADOS  medio-->
     <!-- ======================================================================= -->


     <!-- ======================================================================= -->
     <!--TRABALHOS  REALIZADOS    -->
     <!-- ======================================================================= -->
    <div class="col-xs-12 trabalho_realizados top20 bottom50">
      
		<div class="flexslider">
          <ul class="slides">
          	<?php
          	$result = $obj_site->select("tb_produtos", "order by rand() limit 6");
          	if (mysql_num_rows($result) > 0) {
          		while ($row = mysql_fetch_array($result)) {
          		?>
				<li>
					<a href="<?php echo Util::caminho_projeto(); ?>/produto/<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]) ?>">
						<?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 538, 216, array("class"=>"input100", "alt"=>"$row[titulo]") ) ?>
						<h1><?php Util::imprime($row[titulo]) ?></h1>
					</a>
				</li>          			
          		<?php
          		}
          	}
          	?>
		  </ul>
      
      		<div class="custom-navigation">
			  <a href="#" class="flex-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
			  <div class="custom-controls-container"></div>
			  <a href="#" class="flex-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
			</div>
      
       </div>
       

    </div>
    <!-- ======================================================================= -->
    <!--TRABALHOS  REALIZADOS    -->
    <!-- ======================================================================= -->





   </div>
 </div>
<!-- ======================================================================= -->
<!-- PRODUTOS    -->
<!-- ======================================================================= -->






<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>
