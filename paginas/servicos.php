<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 6);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 6) ?>
<style>
    .bg-interna{
      background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 143px center no-repeat;
    }
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->

  <div class="container">
    <div class="row top120">

      <div class="col-xs-12">
        <h3>NOSSOS SERVIÇOS
          <span class="clearfix">OS MELHORES MATERIAIS E PROFISSIONAIS</span>
        </h3>
      </div>

  </div>
 </div>

 <!-- ======================================================================= -->
 <!-- SERVICOS GERAL    -->
 <!-- ======================================================================= -->
 <div class="container">
   <div class="row top80 bg_branco_dicas pb50">

     <div class="col-xs-6 top40 produtos_geral">
       <h1>CONFIRA NOSSOS SERVIÇOS</h1>
     </div>

     <div class="clearfix">  </div>



     <?php
     $result = $obj_site->select("tb_servicos");
     if (mysql_num_rows($result) > 0) {
         while ($row = mysql_fetch_array($result)) {
         ?>
             <div class="col-xs-4 text-center top30">
               <div class="servicos_tipos">
                 <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]) ?>">

                   <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 98, 101, array("class"=>"", "alt"=>"$row[titulo]") ) ?>

                   <div class="top15">
                     <h1><?php Util::imprime($row[titulo]) ?></h1>
                   </div>
                   <div class="top10 pg10 bottom20">
                     <p><?php Util::imprime($row[descricao], 400) ?></p>
                   </div>
                 </a>
               </div>
             </div>
         <?php
         }
     }
     ?>







 </div>
</div>
<!-- ======================================================================= -->
<!-- SERVICOS GERAL    -->
<!-- ======================================================================= -->



<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>
