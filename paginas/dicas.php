<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 2) ?>
<style>
    .bg-interna{
      background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 143px center no-repeat;
    }
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->







  <div class="container">
    <div class="row top120">

      <div class="col-xs-12">
        <h3>NOSSAS DICAS
          <span class="clearfix">CONTEÚDOS PARA AJUDAR SUA ESCOLHA</span>
        </h3>
      </div>

  </div>
 </div>

 <!-- ======================================================================= -->
 <!-- DICAS GERAL    -->
 <!-- ======================================================================= -->
 <div class="container">
   <div class="row top80 bg_branco_dicas">

     <div class="col-xs-5 top40 dicas_geral">
       <h1>ÚLTIMAS DICAS</h1>
     </div>

     <!-- ======================================================================= -->
     <!-- BARRA PESQUISAR   -->
     <!-- ======================================================================= -->
     <div class="col-xs-7 pesquisa top30">

     	<form action="<?php echo Util::caminho_projeto(); ?>/dicas/" method="post">
		  <div class="input-group">
		   <input type="text" name="busca_dica" class="form-control" placeholder="PESQUISAR DICAS">
		   <span class="input-group-btn">
			 <button class="btn btn-danger" type="submit">OK</button>
		   </span>
		 </div>
		</form>
    </div>
     <!-- ======================================================================= -->
     <!-- BARRA PESQUISAR   -->
     <!-- ======================================================================= -->

     <div class="clearfix">  </div>





     <!-- ======================================================================= -->
     <!-- item 01  -->
     <!-- ======================================================================= -->
     <?php
     //  FILTRA PELO TITULO
     if(isset($_POST[busca_dicas]) and !empty($_POST[busca_dicas]) ):
       $complemento = "AND titulo LIKE '%$_POST[busca_dicas]%'";
     endif;


     $result = $obj_site->select("tb_dicas",$complemento);
     if (mysql_num_rows($result) > 0) {
       $i = 0;
       while($row = mysql_fetch_array($result)){
         ?>

     		<div class="col-xs-4 dicas_home top50">
				<a href="<?php echo Util::caminho_projeto(); ?>/dica/<?php Util::imprime($row[url_amigavel]) ?>" title="">
					<?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 360, 237, array("class"=>"input100", "alt"=>"$row[titulo]") ) ?>
				</a>
				<h1 class="top10"><?php Util::imprime($row[titulo]) ?></h1>

				<a href="<?php echo Util::caminho_projeto(); ?>/dica/<?php Util::imprime($row[url_amigavel]) ?>" class="btn btn_saiba_mais top15" >
				  SAIBA MAIS
				</a>

			  </div>
        <?php
        if ($i == 2) {
          echo '<div class="clearfix"></div>';
          $i = 0;
        }else{
          $i++;
        }
      }
    }
    ?>
      <!-- ======================================================================= -->
      <!-- item 01    -->
      <!-- ======================================================================= -->




 </div>
</div>
<!-- ======================================================================= -->
<!-- DICAS GERAL    -->
<!-- ======================================================================= -->



<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>
