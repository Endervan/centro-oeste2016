<div class="clearfix"></div>
<div class="container-fluid rodape">
	<div class="row">


		<div class="container top40">
			<div class="row">

				<!-- ======================================================================= -->
				<!-- MENU    -->
				<!-- ======================================================================= -->
				<div class="col-xs-12">
					<div class="barra_branca">
					<ul class="menu-rodape">
						<li><a class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/">HOME</a></li>
						<li><a class="<?php if(Url::getURL( 0 ) == "a-unaflor"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/empresa">A EMPRESA</a></li>
						<li><a class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS</a></li>
						<li><a class="<?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS</a></li>
					     <li><a class="<?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/dicas">DICAS</a></li>
						<li><a class="<?php if(Url::getURL( 0 ) == "contatos"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/fale-conosco">FALE CONOSCO</a></li>
						<li><a class="<?php if(Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">TRABALHE CONOSCO</a></li>
					</ul>
					</div>
				</div>
				<!-- ======================================================================= -->
				<!-- MENU    -->
				<!-- ======================================================================= -->


				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->
				<div class="col-xs-3 top30">
					<a href="#">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="" />
					</a>
				</div>
				<!-- ======================================================================= -->
                <!-- LOGO    -->
                <!-- ======================================================================= -->



				<!-- ======================================================================= -->
				<!-- ENDERECO E TELEFONES    -->
				<!-- ======================================================================= -->
				<div class="col-xs-7 padding0 top20">
					<p class="bottom15"><i class="glyphicon glyphicon-home right15"></i><?php Util::imprime($config[endereco]); ?></p>
					<p class="bottom15">
						<i class="glyphicon glyphicon-earphone right15"></i>
						<?php Util::imprime($config[ddd1]); ?>
						<?php Util::imprime($config[telefone1]); ?>
                        <span class="left15"></span>
						<?php if (!empty($config[telefone2])) { ?>
							 <?php Util::imprime($config[ddd2]); ?>
							 <?php Util::imprime($config[telefone2]); ?>
						<?php } ?>
					</p>

					<div class="cartoes">
						<p>
							FORMAS DE PAGAMENTOS :
							<i class="fa fa-cc-visa fa-2x"></i>
							<i class="fa fa-cc-mastercard fa-2x"></i>
							<img src="./imgs/boleto.png" alt="" />
							<img src="./imgs/icon-cheque.gif" alt="" />

							<img class="destaques" src="./imgs/cartao_bnds.jpg" alt="" />

						</p>

					</div>
				</div>
				<!-- ======================================================================= -->
                <!-- ENDERECO E TELEFONES    -->
                <!-- ======================================================================= -->


				<div class="col-xs-2 text-right top25">
					<?php if ($config[google_plus] != "") { ?>
					<a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" >
						<i class="fa fa-google-plus right15" style="color: #000"></i>
					</a>
					<?php } ?>

					<a href="http://www.homewebbrasil.com.br" target="_blank">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo-homeweb.png"  alt="">
					</a>
				</div>

			</div>
		</div>
	</div>
</div>






<div class="container-fluid">
	<div class="row rodape-preto">
		<div class="col-xs-12 text-center top15 bottom15">
			<h5>© Copyright CENTRO OESTE</h5>
		</div>
 </div>
</div>
