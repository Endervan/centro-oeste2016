

<div class="container-fluid topo">
 <div class="row">

	<div class="container">
	  <div class="row">
	    <!--  ==============================================================  -->
		<!-- LOGO -->
		<!--  ==============================================================  -->
		<div class="col-xs-3 top10">
			<a href="<?php echo Util::caminho_projeto() ?>/" title="HOME">
				<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="">
			</a>
		</div>
		<!--  ==============================================================  -->
		<!-- LOGO -->
		<!--  ==============================================================  -->

		<!--  ==============================================================  -->
		<!-- CONTATOS -->
		<!--  ==============================================================  -->
		<div class="col-xs-6 contatos_topo top25 padding0">
			<div class="media pull-right">
			    <div class="media-left media-middle">
			            <img class="media-object top10" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_atendimento.png" alt="">
			    </div>
			    <div class="media-body">
			        <h5 class="media-heading">
			        	<span><?php Util::imprime($config[ddd1]) ?></span><?php Util::imprime($config[telefone1]) ?>

			        	<?php if(!empty($config[telefone2])): ?>
			        		<span><i class="fa fa-whatsapp right5"></i><?php Util::imprime($config[ddd2]) ?></span><?php Util::imprime($config[telefone2]) ?>
			        	<?php endif; ?>
					</h5>

			    </div>
			</div>
		</div>
		<!--  ==============================================================  -->
		<!-- CONTATOS -->
		<!--  ==============================================================  -->

		<!--  ==============================================================  -->
        <!--CARRINHO-->
        <!--  ==============================================================  -->
        <div class="col-xs-3 dropdown carrinho_topo top5 bottom10">
            <button class="btn btn_carrinho input100" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="badge"><?php echo count($_SESSION[solicitacoes_produtos]) + count($_SESSION[solicitacoes_servicos]); ?></span>
            </button>


            <div class="dropdown-menu topo-meu-orcamento pull-right" aria-labelledby="dropdownMenu1">

              <!--  ==============================================================  -->
                  <!--sessao adicionar produtos-->
                  <!--  ==============================================================  -->
                <?php
                if(count($_SESSION[solicitacoes_produtos]) > 0)
                {
                    for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
                    {
                        $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                        ?>
                        <div class="lista-itens-carrinho">
                            <div class="col-xs-2">
                                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                            </div>
                            <div class="col-xs-8">
                                <h1><?php Util::imprime($row[titulo]) ?></h1>
                            </div>
                            <div class="col-xs-1">
                                <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
                            </div>
                        </div>
                        <?php
                                        }
                                    }
                                    ?>


                                    <!--  ==============================================================  -->
                                        <!--sessao adicionar servicos-->
                                        <!--  ==============================================================  -->
                                    <?php
                                    if(count($_SESSION[solicitacoes_servicos]) > 0)
                                    {
                                        for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
                                        {
                                            $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                                            ?>
                                            <div class="lista-itens-carrinho">
                                                <div class="col-xs-2">
                                                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                                                </div>
                                                <div class="col-xs-8">
                                                    <h1><?php Util::imprime($row[titulo]) ?></h1>
                                                </div>
                                                <div class="col-xs-1">
                                                    <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
                                                </div>
                                            </div>
                                            <?php
                                                            }
                                                        }
                                                        ?>




                <div class="text-right bottom20">
                    <a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="ENVIAR ORÇAMENTO" class="btn btn-vermelho" >
                        ENVIAR ORÇAMENTO
                    </a>
                </div>
            </div>
        </div>
        <!--  ==============================================================  -->
        <!--CARRINHO-->
        <!--  ==============================================================  -->



	 </div>
	</div>

 </div>
</div>



<div class="container-fluid bg_preto">
 <div class="row">


	<div class="container">
	 <div class="row">

        <!--  ==============================================================  -->
        <!--  ==============================================================  -->
        <!-- MENU-->
        <!--  ==============================================================  -->
        <div class="col-xs-12 top20 bottom20">
            <div class="menu_topo">
                <ul>
                    <li class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>">
                        <a href="<?php echo Util::caminho_projeto() ?>/">HOME</a>
                    </li>

                    <li class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>">
                        <a href="<?php echo Util::caminho_projeto() ?>/empresa">A EMPRESA</a>
                    </li>

                    <li class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto"){ echo "active"; } ?>">
                        <a href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS</a>
                    </li>

                    <li class="<?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico"){ echo "active"; } ?>">
                        <a href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS</a>
                    </li>


                    <li class="<?php if(Url::getURL( 0 ) == "dicas" or Url::getURL( 0 ) == "dica"){ echo "active"; } ?>">
                        <a href="<?php echo Util::caminho_projeto() ?>/dicas">DICAS</a>
                    </li>

                    <li class="<?php if(Url::getURL( 0 ) == "faleConosco"){ echo "active"; } ?>">
                        <a href="<?php echo Util::caminho_projeto() ?>/fale-conosco">FALE CONOSCO</a>
                    </li>

                    <li class="<?php if(Url::getURL( 0 ) == "trabalheConosco"){ echo "active"; } ?>">
                        <a href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">TRABALHE CONOSCO</a>
                    </li>

                </ul>
            </div>
        </div>
        <!--  ==============================================================  -->
        <!-- MENU-->
        <!--  ==============================================================  -->



		</div>
	</div>


</div>
</div>
