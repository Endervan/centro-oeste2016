
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 21) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 179px center  no-repeat;
  }
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



	<!--  ==============================================================  -->
  <!--  TITULO PAGINA -->
  <!--  ==============================================================  -->
  <div class="container">
     <div class="row top150">

       <div class="col-xs-12 text-right titulo_produtos_geral">
         <h3>NOSSA EMPRESA</h3>

       </div>

   </div>
  </div>
  <!--  ==============================================================  -->
  <!--  TITULO PAGINA -->
  <!--  ==============================================================  -->



  <!--  ==============================================================  -->
  <!--  MENU EMPRESA -->
  <!--  ==============================================================  -->
  <div class="container">
      <div class="row">
          <div class="col-xs-9 col-xs-offset-3">
              <ul class="sub-menu-dentro">
                  <li>
                      <a href="javascript:void(0);" onclick="$('html,body').animate({scrollTop: $('.loc-quem-somos').offset().top}, 2000);" title="QUEM SOMOS">
                          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_empresa01.png" alt="QUEM SOMOS">
                          <br>
                          QUEM SOMOS
                      </a>
                  </li>
                  <li>
                      <a href="javascript:void(0);" onclick="$('html,body').animate({scrollTop: $('.loc-servicos').offset().top}, 2000);" title="SERVIÇO DE ATENDIMENTO">
                          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_empresa02.png" alt="SERVIÇO DE ATENDIMENTO">
                          <br>
                          ATENDIMENTO
                      </a>
                  </li>



									<li>
                      <a href="javascript:void(0);" onclick="$('html,body').animate({scrollTop: $('.loc-clientes').offset().top}, 2000);" title="CLIENTES">
                          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_empresa05.png" alt="CLIENTES">
                          <br>
                          CLIENTES
                      </a>
                  </li>
              </ul>
          </div>
      </div>
  </div>
  <!--  ==============================================================  -->
  <!--  MENU EMPRESA -->
  <!--  ==============================================================  -->



	<!-- ======================================================================= -->
	<!-- DICAS GERAL    -->
	<!-- ======================================================================= -->
	<div class="container">
			<div class="row top40 ">

						<div class="col-xs-12 top35 loc-quem-somos">
								<img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_grande_empresa01.png" alt="QUEM SOMOS" class="pull-left right10">
								<h5>QUEM SOMOS</h5>

								<?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
								<p class="top20"><?php Util::imprime($row[descricao]) ?></p>

						</div>


						<div class="col-xs-12 top35 loc-servicos">
								<img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_grande_empresa02.png" alt="SERVIÇO DE ATENDIMENTO" class="top5 pull-left right10">
								<h5>SERVIÇO DE ATENDIMENTO</h5>

								<?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2) ?>
								<p class="top20"><?php Util::imprime($row[descricao]) ?></p>

						</div>





						<div class="col-xs-12 top35 loc-clientes">
								<img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_grande_empresa03.png" alt="CLIENTES" class="pull-left right10">
								<h5>CLIENTES</h5>
								<?php require_once('../includes/slider_clientes.php'); ?>
						</div>



			</div>
	</div>





<?php require_once('../includes/rodape.php'); ?>

</body>

</html>
