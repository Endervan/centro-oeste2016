
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 12) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 180px center  no-repeat;
  }
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->

	<div class="container">
    <div class="row top160">

      <div class="col-xs-12">
        <h3>NOSSAS DICAS
          <span class="clearfix">CONTEÚDOS PARA AJUDAR SUA ESCOLHA</span>
        </h3>
      </div>

  </div>
 </div>



	<!-- ======================================================================= -->
	<!--DICAS HOME   -->
	<!-- ======================================================================= -->
	<div class="container top70">
		 <div class="row">

			 <!-- ======================================================================= -->
			 <!-- item 01   max 02 itens  -->
			 <!-- ======================================================================= -->

			 <?php
			 $result = $obj_site->select("tb_dicas","order by rand()");
			 if (mysql_num_rows($result) > 0) {
				 while ($row = mysql_fetch_array($result)) {
				 ?>

			 <div class="top10"> </div>
			 <div class="col-xs-6  top25">
				 <a href="<?php echo Util::caminho_projeto() ?>/mobile/dica/<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]) ?>">
					 <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 360, 237, array("class"=>"input100", "alt"=>"$row[titulo]") ) ?>
				 </a>
			 </div>
			 <div class="col-xs-6 dicas_home top20">
				 <h1><?php Util::imprime($row[titulo]) ?></h1>

				 <a class="btn btn_saiba_mais top15" href="<?php echo Util::caminho_projeto() ?>/mobile/dica/<?php Util::imprime($row[url_amigavel]) ?>" >
					 SAIBA MAIS
				 </a>
			 </div>

			 <?php
			 }
		 }
		 ?>
				<!-- ======================================================================= -->
				<!-- item 01    -->
				<!-- ======================================================================= -->


		 </div>
	</div>
	<!-- ======================================================================= -->
	<!-- DICAS HOME   -->
	<!-- ======================================================================= -->


<?php require_once('../includes/rodape.php'); ?>

</body>

</html>
