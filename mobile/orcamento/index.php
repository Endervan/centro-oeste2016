<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 5);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


//  EXCLUI UM ITEM
if(isset($_GET[action]))
{
    //  SELECIONO O TIPO
    switch($_GET[tipo])
    {
        case "produto":
            $id = $_GET[id];
            unset($_SESSION[solicitacoes_produtos][$id]);
            sort($_SESSION[solicitacoes_produtos]);
        break;
        case "servico":
            $id = $_GET[id];
            unset($_SESSION[solicitacoes_servicos][$id]);
            sort($_SESSION[solicitacoes_servicos]);
        break;
        case "piscina_vinil":
            $id = $_GET[id];
            unset($_SESSION[piscina_vinil][$id]);
            sort($_SESSION[piscina_vinil]);
        break;
    }

}
?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 20) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 179px center  no-repeat;
  }
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->

<form class="form-inline FormContatos" role="form" method="post" enctype="multipart/form-data">


	<div class="container">
    <div class="row top160">

      <div class="col-xs-12">
        <h3>ENVIE SEU ORÇAMENTO
          <span class="clearfix">TEREMOS PRAZER EM ATENDÊ-LO</span>
        </h3>
      </div>

    <div class="clearfix"></div>
    <div class="top80"></div>
 	 <div class="col-xs-6 contatos_topo">
 		 <!-- ======================================================================= -->
 		 <!-- CONTATOS  -->
 		 <!-- ======================================================================= -->
 		 <div class="media pull-right">
 			 <div class="media-body">
 				 <h2 class="media-heading left5">
 					 <?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?>
 				 </h2>
 			 </div>
 			 <a class="media-left media-middle btn btn_topo"
       href="tel+55<?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?>">
 				 CHAMAR
 			 </a>
 		 </div>
 		 <!-- ======================================================================= -->
 		 <!-- CONTATOS  -->
 		 <!-- ======================================================================= -->
      </div>

 		 <div class="col-xs-6 contatos_topo">
 			 <!-- ======================================================================= -->
 			 <!-- CONTATOS  -->
 			 <!-- ======================================================================= -->
 			 <div class="media pull-right">
 				 <div class="media-body">
 					 <h2 class="media-heading left5">
 						 <?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>
 					 </h2>
 				 </div>
 				 <a class="media-left media-middle btn btn_topo"
          href="tel+55<?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>">
 					 CHAMAR
 				 </a>
 			 </div>
 			 <!-- ======================================================================= -->
 			 <!-- CONTATOS  -->
 			 <!-- ======================================================================= -->
 	     </div>

			 <?php
       //  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
       if(isset($_POST[nome])){

         //  CADASTRO OS PRODUTOS SOLICITADOS
         for($i=0; $i < count($_POST[qtd]); $i++){
             $dados = $obj_site->select_unico("tb_produtos", "idproduto", $_POST[idproduto][$i]);

             $produtos .= "
                         <tr>
                             <td><p>". $_POST[qtd][$i] ."</p></td>
                             <td><p>". utf8_encode($dados[titulo]) ."</p></td>
                          </tr>
                         ";
         }


 		//  CADASTRO OS SERVICOS SOLICITADOS
         for($i=0; $i < count($_POST[qtd_servico]); $i++){
             $dados = $obj_site->select_unico("tb_servicos", "idservico", $_POST[idservico][$i]);

             $produtos .= "
                         <tr>
                             <td><p>". $_POST[qtd_servico][$i] ."</p></td>
                             <td><p>". utf8_encode($dados[titulo]) ."</p></td>
                          </tr>
                         ";
         }




         //  ENVIANDO A MENSAGEM PARA O CLIENTE
         $texto_mensagem = "
                             O seguinte cliente fez uma solicitação pelo site. <br />

                             Nome: $_POST[nome] <br />
                             Email: $_POST[email] <br />
                             Telefone: $_POST[telefone] <br />
                             Localidade: $_POST[localidade] <br />
                             Mensagem: <br />
                             ". nl2br($_POST[mensagem]) ." <br />

                             <br />
                             <h2> Produtos selecionados:</h2> <br />

                             <table width='100%' border='0' cellpadding='5' cellspacing='5'>
                                 <tr>
                                       <td><h4>QTD</h4></td>
                                       <td><h4>PRODUTO</h4></td>
                                 </tr>
                                 $produtos
                             </table>

                             ";


				Util::envia_email('endvan@gmail.com', utf8_decode("$_POST[nome] solicitou um orçamento"), $texto_mensagem, $nome_remetente, $email);

         Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou um orçamento"), $texto_mensagem, $nome_remetente, $email);
         Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou um orçamento"), $texto_mensagem, $nome_remetente, $email);
         unset($_SESSION[solicitacoes_produtos]);
         unset($_SESSION[solicitacoes_servicos]);
         Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");

       }
       ?>



			 <!-- ======================================================================= -->
			 <!-- CARRINHO  -->
			 <!-- ======================================================================= -->
			 <div class="col-xs-12 tabela_carrinho fale_conosco top30">
				 <h1>ITENS SELECIONADOS</h1>
				 <table class="table">



					 <?php
					 if(count($_SESSION[solicitacoes_produtos]) > 0){
						 for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++){
							 $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
							 ?>
							 <tbody class="col-xs-12 padding0">
								 <tr>
									 <td>
										 <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 66, 65, array("class"=>"", "alt"=>"") ) ?>
									 </td>
									 <td class="col-xs-12">
										 <h2><?php Util::imprime($row[titulo]) ?> <span><?php Util::imprime($row[modelo]) ?></span></h2>
									 </td>
									 <td class="text-center">
										 QDT.<br>
										 <input type="text" class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
										 <input name="idproduto[]" type="hidden" value="<?php echo $row[0]; ?>"  />
									 </td>
									 <td class="text-center">
										 <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir">
											 <i class="fa fa-times-circle fa-2x top20"></i>
										 </a>
									 </td>
								 </tr>
							 </tbody>
							 <?php
						 }
					 }
					 ?>



					 <?php
					 if(count($_SESSION[solicitacoes_servicos]) > 0){
						 for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++){
							 $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
							 ?>
							 <tbody class="col-xs-12 padding0">
								 <tr>
									 <td>
										 <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem_principal]", 65, 65, array("class"=>"", "alt"=>"") ) ?>
									 </td>
									 <td class="col-xs-12">
										 <h2><?php Util::imprime($row[titulo]) ?> <span><?php Util::imprime($row[modelo]) ?></span></h2>
									 </td>
									 <td class="text-center">
										 QDT.<br>
										 <input type="text" class="input-lista-prod-orcamentos" name="qtd_servico[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
										 <input name="idservico[]" type="hidden" value="<?php echo $row[0]; ?>"  />
									 </td>
									 <td class="text-center">
                     <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento/?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir">
											 <i class="fa fa-times-circle fa-2x top20"></i>
										 </a>
									 </td>
								 </tr>
							 </tbody>
							 <?php
						 }
					 }
					 ?>




				 </table>
			 </div>
			 <!-- ======================================================================= -->
			 <!-- CARRINHO  -->
			 <!-- ======================================================================= -->



 			 <div class="col-xs-12 top40 fale_conosco">
 				 <h1>ENVIE UM E-MAIL</h1>
 			 </div>

 </div>
 </div>





 <div class="container">
     <div class="row ">
      <div class="col-xs-12 padding0">

              <div class="top10">
                    <div class="fundo_formulario">
                      <!-- formulario orcamento -->

                        <div class="col-xs-12 top20">
                          <div class="form-group input100 has-feedback">
                            <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">

                          </div>
                        </div>

                        <div class="col-xs-12 top20">
                          <div class="form-group  input100 has-feedback">
                            <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">

                          </div>
                        </div>


                        <div class="col-xs-12 top20">
                          <div class="form-group  input100 has-feedback">
                            <input type="text" name="telefone" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">

                          </div>
                        </div>

                        <div class="col-xs-12 top20">
                          <div class="form-group  input100 has-feedback">
                            <input type="text" name="assunto" class="form-control fundo-form1 input-lg input100" placeholder="ASSUNTO">

                          </div>
                        </div>

                        <div class="col-xs-12 top20">
                         <div class="form-group input100 has-feedback">
                          <textarea name="mensagem" cols="30" rows="9" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>

                        </div>
                      </div>

                    <!-- formulario orcamento -->
                    <div class="col-xs-12 text-right">
                      <div class="top15">
                        <button type="submit" class="btn btn-formulario" name="btn_contato">
                          ENVIAR
                        </button>
                      </div>
                    </div>

                  </div>


                  <!--  ==============================================================  -->
                  <!-- FORMULARIO-->
                  <!--  ==============================================================  -->


            </div>
            </div>


    </div>
  </div>
</form>


 <?php require_once('../includes/rodape.php'); ?>

 </body>

 </html>




 <script>
   $(document).ready(function() {
     $('.FormContatos').bootstrapValidator({
       message: 'This value is not valid',
       feedbackIcons: {
         valid: 'glyphicon glyphicon-ok',
         invalid: 'glyphicon glyphicon-remove',
         validating: 'glyphicon glyphicon-refresh'
       },
       fields: {
        nome: {
         validators: {
           notEmpty: {

           }
         }
       },
       email: {
         validators: {
           notEmpty: {

           },
           emailAddress: {
             message: 'Esse endereço de email não é válido'
           }
         }
       },
       telefone: {
          validators: {
            notEmpty: {

            },
            phone: {
                country: 'BR',
                message: 'Informe um telefone válido.'
            }
          },
        },
       assunto: {
         validators: {
           notEmpty: {

           }
         }
       },
       cidade: {
         validators: {
           notEmpty: {

           }
         }
       },
       estado: {
         validators: {
           notEmpty: {

           }
         }
       },
       curriculo: {
         validators: {
           notEmpty: {
             message: 'Por favor insira seu currículo'
           },
           file: {
             extension: 'doc,docx,pdf,rtf',
             type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                             maxSize: 5*1024*1024,   // 5 MB
                             message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                           }
                         }
                       },
                       mensagem: {
                         validators: {
                           notEmpty: {

                           }
                         }
                       }
                     }
                   });
   });
 </script>
