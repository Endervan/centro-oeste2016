<!-- ======================================================================= -->
<!-- CONTATOS  -->
<!-- ======================================================================= -->
<div class="media pull-right">
  <div class="media-body">
    <h2 class="media-heading left2">
      <?php Util::imprime ($config[ddd1]); ?>
      <?php Util::imprime ($config[telefone1]); ?>
    </h2>
  </div>
  <a class="media-left media-middle btn btn_topo" href="tel+55">
    CHAMAR
  </a>
</div>

<?php if (!empty($config[telefone2])): ?>
<div class="media pull-right">
  <div class="media-body">
    <h2 class="media-heading left5">
      <?php Util::imprime ($config[ddd2]); ?>
      <?php Util::imprime ($config[telefone2]); ?>
    </h2>
  </div>
  <a class="media-left media-middle btn btn_topo" href="tel+55">
      CHAMAR
  </a>
</div>
<?php endif ?>
<!-- ======================================================================= -->
<!-- CONTATOS  -->
<!-- ======================================================================= -->
