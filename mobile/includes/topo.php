<div class="container">
  <div class="row">

   <div class="bg_cinza_topo">  </div>

    <div class="col-xs-5 top25">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile" title="" class="logo left15">
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="">
          </a>
    </div>


    <div class="col-xs-7 top10 contatos_topo">
      <!-- ======================================================================= -->
      <!-- CONTATOS  -->
      <!-- ======================================================================= -->
      <div class="media pull-right">
        <div class="media-body">
          <h2 class="media-heading left5">
            <i class="fa fa-phone"></i>
            <?php Util::imprime ($config[ddd1]); ?> <?php Util::imprime ($config[telefone1]); ?>
          </h2>
        </div>
        <a class="media-left media-middle btn btn_topo"
        href="tel:+55<?php Util::imprime ($config[ddd1]); ?> <?php Util::imprime ($config[telefone1]); ?>">
          CHAMAR
        </a>
      </div>

     <?php if (!empty($config[telefone2])): ?>
      <div class="media pull-right">
        <div class="media-body">
          <h2 class="media-heading left5">
            <i class="fa fa-whatsapp"></i>
            <?php Util::imprime ($config[ddd2]); ?> <?php Util::imprime ($config[telefone2]); ?>
          </h2>
        </div>
        <a class="media-left media-middle btn btn_topo"
         href="tel:+55<?php Util::imprime ($config[ddd2]); ?><?php Util::imprime ($config[telefone2]); ?>">
            CHAMAR
        </a>
      </div>
      <?php endif ?>
      <!-- ======================================================================= -->
      <!-- CONTATOS  -->
      <!-- ======================================================================= -->

    </div>

  </div>
</div>



<!-- ======================================================================= -->
<!-- MENU  -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row">
    <div class="col-xs-12 top15 padding0 bg_menu">

        <!-- ======================================================================= -->
        <!-- MENU  -->
        <!-- ======================================================================= -->
        <div class="col-xs-5">
          <select name="menu" id="menu-site" class="select-menu top25">
            <option value=""></option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/">HOME</option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/empresa">EMPRESA</option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/dicas">DICAS</option>
             <option value="<?php echo Util::caminho_projeto() ?>/mobile/produtos">PRODUTOS</option>
             <option value="<?php echo Util::caminho_projeto() ?>/mobile/servicos">SERVIÇOS</option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/fale-conosco">FALE CONOSCO</option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/trabalhe-conosco">TRABALHE CONOSCO</option>

          </select>
        </div>
        <!-- ======================================================================= -->
        <!-- MENU  -->
        <!-- ======================================================================= -->


        <!-- ======================================================================= -->
        <!-- PESQUISAS-->
        <!-- ======================================================================= -->
        <div class=" col-xs-2 dropdown text-center padding0">
          <a href="#" class="dropdown-toggle btn btn_pesquisas_topo" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-search right10"></i>
            <span class="caret"></span></a>

            <ul class="dropdown-menu form_busca_topo pull-right">
              <form class="navbar-form navbar-left" action="<?php echo Util::caminho_projeto() ?>/mobile/produtos/" method="post">
                <div class="form-group col-xs-8">
                  <input type="text" class="form-control" placeholder="O que está procurando?" name="busca_produtos">
                </div>
                <div class="col-xs-4 padding0">
                    <button type="submit" class="btn btn_topo top3">Buscar</button>
                </div>

              </form>
            </ul>
          </div>
          <!-- ======================================================================= -->
          <!-- PESQUISAS-->
          <!-- ======================================================================= -->





          <!-- ======================================================================= -->
          <!-- botao carrinho de compra -->
          <!-- ======================================================================= -->
          <div class="col-xs-5 dropdown">
            <a class="btn btn_carrinho" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/btn_carrinho.png" alt="" />
            </a>

            <div class="dropdown-menu topo-meu-orcamento pull-right" aria-labelledby="dLabel">

              <?php
              if(count($_SESSION[solicitacoes_produtos]) > 0)
              {
               for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
               {
                 $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                 ?>



                 <div class="lista-itens-carrinho col-xs-12">
                   <div class="col-xs-2">
                     <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"")) ?>
                   </div>
                   <div class="col-xs-8">
                     <h1><?php Util::imprime($row[titulo]) ?></h1>
                   </div>
                   <div class="col-xs-1">
                     <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
                   </div>
                 </div>
                 <?php
               }
             }



             if(count($_SESSION[solicitacoes_servicos]) > 0)
             {


               for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
               {
                 $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                 ?>
                 <div class="lista-itens-carrinho col-xs-12 top10">
                   <div class="col-xs-2">
                     <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"")) ?>
                   </div>
                   <div class="col-xs-8">
                     <h1><?php Util::imprime($row[titulo]) ?></h1>
                   </div>
                   <div class="col-xs-1">
                     <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento/?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
                   </div>
                 </div>
                 <?php
               }
             }

             ?>

            <div class="text-right top10 bottom10 pg10">
              <a class="btn btn_topo" href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento" title="Finalizar" >
                FINALIZAR
              </a>
            </div>
          </div>
        </div>
        <!-- ======================================================================= -->
        <!-- botao carrinho de compra -->
        <!-- ======================================================================= -->

    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- MENU  -->
<!-- ======================================================================= -->
