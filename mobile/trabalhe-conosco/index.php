
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 19) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 179px center  no-repeat;
  }
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->

	<div class="container">
    <div class="row top160">

      <div class="col-xs-7 titulo_fale">
        <h4>ENTRE EM CONTATO SEMPRE QUE PRECISAR
          <span class="clearfix">FALE CONOSCO</span>
        </h4>
      </div>

  </div>
 </div>

<div class="container">
<div class="row top55">

	 <div class="col-xs-6 contatos_topo">
		 <!-- ======================================================================= -->
		 <!-- CONTATOS  -->
		 <!-- ======================================================================= -->
		 <div class="media pull-right">
			 <div class="media-body">
				 <h2 class="media-heading left5">
					 <?php Util::imprime ($config[ddd1]); ?>
					 <?php Util::imprime ($config[telefone1]); ?>
				 </h2>
			 </div>
			 <a class="media-left media-middle btn btn_topo" href="tel+55">
				 CHAMAR
			 </a>
		 </div>
		 <!-- ======================================================================= -->
		 <!-- CONTATOS  -->
		 <!-- ======================================================================= -->
     </div>

		 <div class="col-xs-6 contatos_topo">
			 <!-- ======================================================================= -->
			 <!-- CONTATOS  -->
			 <!-- ======================================================================= -->
			 <div class="media pull-right">
				 <div class="media-body">
					 <h2 class="media-heading left5">
						 <?php Util::imprime ($config[ddd1]); ?>
						 <?php Util::imprime ($config[telefone1]); ?>
					 </h2>
				 </div>
				 <a class="media-left media-middle btn btn_topo" href="tel+55">
					 CHAMAR
				 </a>
			 </div>
			 <!-- ======================================================================= -->
			 <!-- CONTATOS  -->
			 <!-- ======================================================================= -->
	     </div>

			 <div class="col-xs-12 top40 fale_conosco">
				 <h1>ENVIE UM E-MAIL</h1>
			 </div>

</div>
</div>



<div class="container">
    <div class="row ">
     <div class="col-xs-12 padding0">




             <div class="top10">

							 <?php
						 //  VERIFICO SE E PARA ENVIAR O EMAIL
						 if(isset($_POST[nome]))
						 {

							 if(!empty($_FILES[curriculo][name])):
								 $nome_arquivo = Util::upload_arquivo("../../uploads", $_FILES[curriculo]);
								 $texto = "Anexo: ";
								 $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
								 $texto .= "<a href='".Util::caminho_projeto()."../../uploads/$nome_arquivo' target='_blank'>".Util::caminho_projeto()."../../uploads/$nome_arquivo</a>";
							 endif;

										 $texto_mensagem = "
																			 Nome: ".$_POST[nome]." <br />
																			 Telefone: ".$_POST[telefone]." <br />
																			 Email: ".$_POST[email]." <br />
																			 Escolaridade: ".$_POST[escolaridade]." <br />
																			 Cargo: ".$_POST[cargo]." <br />
																			 Área: ".$_POST[area]." <br />
																			 Cidade: ".$_POST[cidade]." <br />
																			 Estado: ".$_POST[estado]." <br />
																			 Mensagem: <br />
																			 ".nl2br($_POST[mensagem])."

																			 <br><br>
																			 $texto
																			 ";


										 Util::envia_email($config[email], utf8_decode("$_POST[nome] enviou um currículo"), $texto_mensagem, utf8_decode($_POST[nome]), ($_POST[email]));
										 Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] enviou um currículo"), $texto_mensagem, utf8_decode($_POST[nome]), ($_POST[email]));
										 Util::alert_bootstrap("Obrigado por entrar em contato.");
										 unset($_POST);
						 }

						 ?>


                 <form class="form-inline FormCurriculo" role="form" method="post" enctype="multipart/form-data">
                   <div class="fundo_formulario">
                     <!-- formulario orcamento -->

                       <div class="col-xs-12 top20">
                         <div class="form-group input100 has-feedback">
                           <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">

                         </div>
                       </div>

                       <div class="col-xs-12 top20">
                         <div class="form-group  input100 has-feedback">
                           <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">

                         </div>
                       </div>


                       <div class="col-xs-12 top20">
                         <div class="form-group  input100 has-feedback">
                           <input type="text" name="telefone" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">

												 </div>
											 </div>


											 <div class="col-xs-12 top20">
												 <div class="form-group input100 has-feedback ">
													 <input type="text" name="escolaridade" class="form-control fundo-form1 input100  input-lg" placeholder="ESCOLARIDADE">

												 </div>
											 </div>



											 <div class="col-xs-12 top20">
												 <div class="form-group input100 has-feedback">
													 <input type="text" name="cargo" class="form-control fundo-form1 input100  input-lg" placeholder="CARGO" >

												 </div>
											 </div>

											 <div class="col-xs-12 top20">
												 <div class="form-group input100 has-feedback ">
													 <input type="text" name="area" class="form-control fundo-form1 input100  input-lg" placeholder="AREA">

												 </div>
											 </div>



											 <div class="col-xs-12 top20">
												 <div class="form-group input100 has-feedback ">
													 <input type="text" name="cidade" class="form-control fundo-form1 input100  input-lg" placeholder="CIDADE">

												 </div>
											 </div>
											 <div class="col-xs-12 top20">
												 <div class="form-group input100 has-feedback ">
													 <input type="text" name="estado" class="form-control fundo-form1 input100  input-lg" placeholder="ESTADO">

												 </div>
											 </div>

											 <div class="col-xs-12 top20">
												 <div class="form-group input100 has-feedback ">
													 <input type="file" name="curriculo" class="form-control fundo-form1 input100  input-lg" placeholder="CURRÍCULO">

												 </div>
											 </div>



										 <div class="col-xs-12 top20">
											 <div class="form-group input100 has-feedback">
												 <textarea name="mensagem" cols="30" rows="9" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>

											 </div>
										 </div>


                   <!-- formulario orcamento -->
                   <div class="col-xs-12 text-right">
                     <div class="top15">
                       <button type="submit" class="btn btn-formulario" name="btn_contato">
                         ENVIAR
                       </button>
                     </div>
                   </div>

                 </div>


                 <!--  ==============================================================  -->
                 <!-- FORMULARIO-->
                 <!--  ==============================================================  -->
               </form>

           </div>
           </div>

           <!--  ==============================================================  -->
           <!--MAPA-->
           <!--  ==============================================================  -->
           <div class="col-xs-12 top20">
             <div class="top50 bottom20 fale_conosco">
               <h1>COMO CHEGAR</h1>
							 <div class="top30 bottom25">
								 	<p>
							 	<?php Util::imprime ($config[endereco]); ?>
							 </p>
						 </div>
             </div>
             <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="371" frameborder="0" style="border:0" allowfullscreen></iframe>
           </div>
           <!--  ==============================================================  -->
           <!--MAPA-->
           <!--  ==============================================================  -->
   </div>
 </div>



<?php require_once('../includes/rodape.php'); ?>

</body>

</html>



<script>
  $(document).ready(function() {
    $('.FormCurriculo').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
         validators: {
           notEmpty: {

           },
           phone: {
               country: 'BR',
               message: 'Informe um telefone válido.'
           }
         },
       },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5*1024*1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                          }
                        }
                      },
                      mensagem: {
                        validators: {
                          notEmpty: {

                          }
                        }
                      }
                    }
                  });
  });
</script>
