
<?php
require_once("../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>
<!doctype html>
<html>

<head>
  <?php require_once('./includes/head.php'); ?>

  <script>
  $(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: true,
    itemWidth: 240,
    itemMargin: 0,
    controlsContainer: $(".custom-controls-container"),
      customDirectionNav: $(".custom-navigation a")
  });
  });
  </script>




</head>

<body>


    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/topo.php') ?>
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!-- BG HOME    -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row carroucel-home">

     <div class="bg_branco"></div>
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!--  ==============================================================  -->
        <!-- ARRUSTE BOLINHAS E SETA DENTRO GRID PARA QLQ RESOLUCAO-->
        <!--  ==============================================================  -->
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <!-- Indicators -->
              <ol class="carousel-indicators">
                <?php
                $result = $obj_site->select("tb_banners", "and tipo_banner = 2 ");
                if(mysql_num_rows($result) > 0){
                  $i = 0;
                  while ($row = mysql_fetch_array($result)) {
                    $imagens[] = $row;
                    ?>
                    <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){ echo "active"; } ?>"></li>
                    <?php
                    $i++;
                  }
                }
                ?>
              </ol>



            </div>
          </div>
        </div>
        <!--  ==============================================================  -->
        <!-- ARRUSTE BOLINHAS E SETA DENTRO GRID PARA QLQ RESOLUCAO-->
        <!--  ==============================================================  -->


        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">


          <?php
          if (count($imagens) > 0) {
            $i = 0;
            foreach ($imagens as $key => $imagem) {
              ?>
              <div class="item <?php if($i == 0){ echo "active"; } ?>">

                <?php if (!empty($imagem[url])): ?>
                  <a href="<?php Util::imprime($imagem[url]); ?>" >
                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="<?php Util::imprime($imagem[titulo]); ?>">
                  </a>
                <?php else: ?>
                  <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="<?php Util::imprime($imagem[titulo]); ?>">
                <?php endif; ?>

              </div>
              <?php
              $i++;
            }
          }
          ?>

        </div>

        <a class="left carousel-control controles_produtos_home" href="#carousel-example-generic" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control controles_produtos_home" href="#carousel-example-generic" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>


      </div>


    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- BG HOME    -->
  <!-- ======================================================================= -->




  	<!--  ==============================================================  -->
  	<!--  MENU EMPRESA -->
  	<!--  ==============================================================  -->
  	<div class="container ">
  			<div class="row relativo">
          <div class="bg_menu_produtos">  </div>
  					<div class="col-xs-12">
  							<ul class="sub-menu-produtos_home">



                  <?php
                  $result = $obj_site->select("tb_categorias_produtos", "order by ordem limit 4");
                  if (mysql_num_rows($result) > 0) {
                    while($row = mysql_fetch_array($result)){
                      ?>
                      <li>
                        <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                          <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>">
                          <span class="clearfix"><?php Util::imprime($row[titulo]); ?></span>
                        </a>
                      </li>
                      <?php
                    }
                  }
                  ?>





  							</ul>
  					</div>
  			</div>
  	</div>
  	<!--  ==============================================================  -->
  	<!--  MENU EMPRESA -->
  	<!--  ==============================================================  -->







   <!-- ======================================================================= -->
   <!-- PRODUTOS HOME    -->
   <!-- ======================================================================= -->
   <div class='container '>
     <div class="row">

       <!-- ======================================================================= -->
       <!-- item 01   max 04 itens  -->
       <!-- ======================================================================= -->
       <?php
         $result = $obj_site->select("tb_produtos", "order by rand() limit 4");
         if(mysql_num_rows($result) > 0){
         while ($row = mysql_fetch_array($result)) {
         ?>
           <div class="col-xs-6 produtos_home top35">
           <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]) ?>" title="">
             <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 270, 270, array("class"=>"input100", "alt"=>"$row[titulo]") ) ?>
           </a>
           <h1 class="top10"><?php Util::imprime($row[titulo]) ?></h1>
           <h2><?php Util::imprime($row[modelo]) ?></h2>

           <a class="btn btn_saiba_mais top15" href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]) ?>" data-toggle="tooltip" data-placement="top" title="Adicionar ao orçamento">
           SAIBA MAIS
           </a>
           <a class="btn btn_orcamentos_home top15 left5" href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'" >
           <i class="fa fa-cart-arrow-down fa-2x " aria-hidden="true"></i>
           </a>
         </div>
         <?php
         }
       }
       ?>

          <!-- ======================================================================= -->
          <!-- item 01    -->
          <!-- ======================================================================= -->



     </div>
   </div>
   <!-- ======================================================================= -->
   <!-- PRODUTOS HOME    -->
   <!-- ======================================================================= -->



   <!-- ======================================================================= -->
   <!--SERVICOS HOME    -->
   <!-- ======================================================================= -->
  <div class="container bg_servicos_home top70">
      <div class="row">

            <div class="col-xs-12 top50">
              <div class="media pull-right">
                <div class="media-body text-right">
                  <h4 class="media-heading">CONFIRA OS
                    <span class="clearfix">NOSSOS SERVIÇOS</span>
                  </h4>
                </div>

                <div class="media-right">
                  <i class="fa fa-angle-double-down fa-5x media-object"></i>
                </div>

              </div>
            </div>


            <!-- ======================================================================= -->
            <!--TIPOS SERVICOS HOME  ate 4 itens home  -->
            <!-- ======================================================================= -->
            <div class="col-xs-12 servicos_home top10">

              <?php
              $result = $obj_site->select("tb_servicos", "order by rand() limit 4");
              if (mysql_num_rows($result) > 0) {
                while ($row = mysql_fetch_array($result)) {
                ?>

                <a href="<?php echo Util::caminho_projeto(); ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]) ?>">
      						<div class="media top15">
      						  <div class="media-left media-middle">
      							  <i class="fa fa-check-square-o media-object"></i>
      						  </div>
      						  <div class="media-body">
      							<p class="media-heading"><?php Util::imprime($row[titulo]) ?></p>
      						  </div>
      						</div>
                 			</a>
                  	<?php
                  	}
                  }
                  ?>


            </div>

            <div class="col-xs-12 servicos_home01">
              <div class="media">
                <div class="media-body text-right">
                  <h4 class="media-heading">CONHEÇA
                    <span class="clearfix">NOSSA EMPRESA</span>
                  </h4>
                </div>

                <div class="media-right">
                  <i class="fa fa-angle-double-down fa-5x media-object"></i>
                </div>
              </div>

              <div class="top5 descricao_servicos">
                  <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3) ?>
                  <p><?php Util::imprime($row[descricao]) ?></p>
              </div>

              <div class="top15 text-right seta_media">
                  <h1>NOSSOS CLIENTES<i class="fa fa-angle-double-down left5"></i></h1>
              </div>

          </div>


      </div>
  </div>
   <!-- ======================================================================= -->
   <!-- SERVICOS HOME    -->
   <!-- ======================================================================= -->


   <!-- ======================================================================= -->
   <!--TRABALHOS  REALIZADOS    -->
   <!-- ======================================================================= -->
   <div class="container">
     <div class="row">
       <div class="trabalho_realizados">

         <div class="flexslider">
           <ul class="slides">
             <?php
             $result = $obj_site->select("tb_clientes", "order by rand() limit 10");
             if (mysql_num_rows($result) > 0) {
               while ($row = mysql_fetch_array($result)) {
                 ?>
                 <li>
                   <a href="<?php echo Util::caminho_projeto(); ?>/mobile/empresa" title="<?php Util::imprime($row[titulo]) ?>">
                     <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 538, 216, array("class"=>"input100", "alt"=>"$row[titulo]") ) ?>
                     <h1 class="left10"><?php Util::imprime($row[titulo]) ?></h1>
                   </a>
                 </li>
                 <?php
               }
             }
             ?>
           </ul>

           <div class="custom-navigation">
             <a href="#" class="flex-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
             <div class="custom-controls-container"></div>
             <a href="#" class="flex-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
           </div>

         </div>


       </div>
     </div>
   </div>
   <!-- ======================================================================= -->
   <!--TRABALHOS  REALIZADOS    -->
   <!-- ======================================================================= -->

   <!-- ======================================================================= -->
   <!--DICAS HOME   -->
   <!-- ======================================================================= -->
   <div class="container top40">
      <div class="row">

        <div class="col-xs-8">
          <div class="media">
            <div class="media-body text-right">
              <h4 class="media-heading">CONFIRA AS
                <span class="clearfix">NOSSAS DICAS</span>
              </h4>
            </div>

            <div class="media-right">
              <i class="fa fa-angle-double-down fa-5x media-object"></i>
            </div>

          </div>
        </div>

        <div class="col-xs-4 top15 text-right">
          <a class="btn btn_saiba_mais top10" href="<?php echo Util::caminho_projeto() ?>/mobile/dicas">
            VER TODAS
          </a>
        </div>
        <div class="clearfix">  </div>




        <!-- ======================================================================= -->
        <!-- item 01   max 02 itens  -->
        <!-- ======================================================================= -->

        <?php
        $result = $obj_site->select("tb_dicas", "order by rand() limit 2");
        if (mysql_num_rows($result) > 0) {
        	while ($row = mysql_fetch_array($result)) {
        	?>

        <div class="top10"> </div>
        <div class="col-xs-6  top25">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile/dica/<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]) ?>">
            <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 360, 237, array("class"=>"input100", "alt"=>"$row[titulo]") ) ?>
          </a>
        </div>
        <div class="col-xs-6 dicas_home top20">
          <h1><?php Util::imprime($row[titulo]) ?></h1>

          <a class="btn btn_saiba_mais top15" href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]) ?>" >
            SAIBA MAIS
          </a>
        </div>

        <?php
        }
      }
      ?>
         <!-- ======================================================================= -->
         <!-- item 01    -->
         <!-- ======================================================================= -->


      </div>
   </div>
   <!-- ======================================================================= -->
   <!-- DICAS HOME   -->
   <!-- ======================================================================= -->





  <!-- ======================================================================= -->
  <!-- RODAPE   -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php'); ?>
  <!-- ======================================================================= -->
  <!-- RODAPE   -->
  <!-- ======================================================================= -->


</body>

</html>
